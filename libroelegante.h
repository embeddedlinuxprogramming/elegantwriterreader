//
// Created by jbgomezm on 11/9/23.
//

#ifndef ELEGANTWRITERREADER_LIBROELEGANTE_H
#define ELEGANTWRITERREADER_LIBROELEGANTE_H

#include <stdatomic.h>

typedef struct {
    char mensaje[80];
    atomic_int lectores;
    atomic_int escritor;
} LibroElegante_t;


int leerLibro(LibroElegante_t* libro);

int escribirLibro(LibroElegante_t* libro);

int leyendo(LibroElegante_t* libro);

int escribiendo(LibroElegante_t* libro);

int finalizarEscritura(LibroElegante_t* libro);

int finalizarLectura(LibroElegante_t* libro);

#endif //ELEGANTWRITERREADER_LIBROELEGANTE_H
