//
// Created by jbgomezm on 11/9/23.
//

#include "libroelegante.h"

int leerLibro(LibroElegante_t* libro) {
    if (libro->escritor) // No se pudo leer.
        return -1;
    libro->lectores++;
    return 0;
}

int escribirLibro(LibroElegante_t* libro) {
    if (libro->escritor || libro->lectores) // No se pudo escribir.
        return -1;
    libro->escritor++;
    return 0;
}

int leyendo(LibroElegante_t* libro) { return libro->lectores; }

int escribiendo(LibroElegante_t* libro) { return libro->escritor; }

int finalizarEscritura(LibroElegante_t* libro) {
    if (libro->escritor) {
        libro->escritor--;
        return 0;
    }
    return -1;
}

int finalizarLectura(LibroElegante_t* libro) {
    if (libro->lectores) {
        libro->lectores--;
        return 0;
    }
    return -1;
}
